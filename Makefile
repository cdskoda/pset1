CC = gcc
CFLAGS = -std=c99 -pedantic -Wall -g3

nfind: nfind.o
	${CC} ${CFLAGS} -o nfind nfind.o

nfind.o: nfind.c
	${CC} ${CFLAGS} -c nfind.c
