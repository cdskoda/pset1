Issues/Things:
    - when passing initial cmd.filenames[i] into recursive helper function, need to check if filenames[i] is file or dir. current helper only works for subdirectories
    - initial filenames[i] will never be printed, but it's contents will if they past tests
        - WvK: maybe fixed? E.g. make; ./nfind -name snip\*   or   ./nfind -name \*.c
    - need to find out how to apply -o
        - WvK: pseudocode = if passed_so_far and then -o then we're done
    - need to figure out how to check for loops while in helper function (see S_ISLINK)
    - WvK: for "nfind filename" the filename can be file or dir
    - CDS: added -newer to applyExpressions and initial filenames[i] testing
    - CDS: beginning to work on symbolic loop check (will probably make a linked list of nodes that carry device ID, inode num, and pointer to previous node, root node's prev will be NULL)
        WvK: the current (work in progress?) gives me compiler issues
        WvK: not sure it works correctly when passing the node list as argument
        WvK: global linked list might be easier? https://cboard.cprogramming.com/c-programming/132035-global-linked-list.html

    - CDS: created node struct to help deal with loop checking. need to simply create a while loop that checks each inode and device number of each node before it until it reaches node
            - need to make while loop check and correctly format and add each node to list of nodes


done
---
    - WvK: depth wasn't implemented correctly
        1) should not decrease maxdepth for each call to the helper function, make it argument in helper function
        2) should separate "passed" from calling helper function recursively
    - when stat()'ing or lstat()'ing -newer arg, always failing
        - WvK: I think this is fixed. You used stat() == 0, should be stat() < 0   :-)
        - WvK: I moved -newer to the parse section, and there I store the max st_mtimespec.tv_sec
    - -exec always saying no \; found even when in argv[]
        - WvK: fixed. The \; is needed so that the SHELL does not treat it as end of command. We simply test for ";"
        - WvK: same for wildcards. When you say -name *.c, the SHELL will expend it before giving it to nfind. 
            To avoid use e.g. -name \*.c or -name '*.c'
    - CDS: fixed "/nfind filename" issue, filename can now be a symbolic link, subdir, or file and nfind functions correctly


main:
---
parse cmdline argv
    including: if !cmd.action then cmd.action = -print

for cmd.countFilenames (each is a path)
    testAndActionsForPath(cmd, path, level=0)
---

testAndActionsForPath(cmd, path, level)
---
    check if level <= cmd.maxdepth
    fp = stat / lstat
    if fp == link 
        if node details are in list: exit
        add node details to list
    if !-depth 
        testAndActionsForFp(cmd,fp)
    if path == dir then 
        foreach file in dir
            if file is not . or ..
                testAndActionsForPath(cmd,file, level+1)
    if -depth 
        testAndActionsForFp(cmd,fp)
---

testAndActionsForFp(cmd, fp)
---
    // node can be file/dir/link(-P)

    passed = true
    if . or .. passed = false                // CDS: this part was moved, need to check for . or .. when I'm going through files within dir in testAndActionsForPath, the fp to root dir "." can be passed, but not the . or .. within other dirs
    for each cmd.test
        -name: passed = fnmatch(...)
        -newer: passed = difftime(...)
        -o: if passed then break
    if !passed return

    for each cmd.action
        -exec:
            replace *each* {} with file
            passed = system()
        -print:
            if passed: print

---
