#!/bin/bash

./nfind -P -L -P -L filename1 filename2 -depth -name *abc -o -newer file1 -o -exec ls -l {} \; -print -maxdepth 1

# ./nfind -P -L -P -L filename1 filename2 -depth -name *abc -o -newer file1 -o -exec ls -l {} \; -print this_should_give_syntax_error_or_be_ignored -maxdepth 1


# test with different -exec delimiter
#./nfind -P -L -P -L filename1 filename2 -depth -name *abc -o -newer file1 -o -exec ls -l {} xx -print this_should_give_syntax_error_or_be_ignored -maxdepth 1

# should give error: 2nd -newer is missing argument
# ./nfind -P -L -P -L filename1 filename2 -depth -name *abc -o -newer file1 -o -newer -exec ls -l {} \; -print this_should_give_syntax_error_or_be_ignored -maxdepth 1

