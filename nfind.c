/*------------------------------------------------------------------
****************************** nfind.c *****************************
--------------------------------------------------------------------
    Name:  Clay Skoda
    netid: cds57
    email: clayton.skoda@yale.edu 
------------------------------------------------------------------*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <fnmatch.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>
// #include <error.h>
#include <errno.h>

// POSIX st_Xtim, macOS st_Xmtimespec
//#define st_mtim st_mtimespec
//#define st_atim st_atimespec



#define DEBUG 1

#define ANSI_COLOR_YELLOW    "\x1b[33m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define debug(fmt, ...) do { if (DEBUG) fprintf(stderr, fmt "\n", __VA_ARGS__); } while (0)
#define WARN(format,...) fprintf (stderr, "nfind: " format "\n", __VA_ARGS__)
#define DIE(format,...)  WARN(format,__VA_ARGS__), exit (EXIT_FAILURE)

// fixed array sizes (ideally: dynamically sized - will deal with later)
#define ARRAYSIZE 100 



// Command Line Stuct (cmdline)
typedef struct cmdline {
    // options
    bool followLinks;               // option flag, determines if we use lstat() (-P, false) or stat() (-L, true)
    bool depth;                     // if true, first traverse through dirs recursively, if false first through cur dir, then recursively through nested dirs
    int maxdepth;                   // number of levels we can traverse recursively from root dir (-1 implies no limit)
    // paths
    time_t newer;          // the newest of the -newer arguments
    char *filenames[ARRAYSIZE];     // array of filenames given in command line
    // tests
    struct {                        // 2xARRAYSIZE matrix
        char *name;                 // expression name (i.e. -name)
        char *argument;             // expression arg (i.e. filename for -name, mtime for -newer, command for -exec)
    } tests[ARRAYSIZE];
    struct {
        char *action;
        char *argument;
    } actions[ARRAYSIZE];
    int countFilenames;             // number filenames in command line
    int countExpressions;           // number -name in command line
    int countActions;               // number actions in command line
} cmdline;

// Node struct for checking for symbolic loops
typedef struct node{
    dev_t devid ;                   // node's device id
    ino_t inodenum ;                // node's inode number
    struct node *prev ;             // pointer to node before node
} node;

// Concatenates an array of strings into single string, used to get -exec arg and helper function
// Source: https://stackoverflow.com/questions/25767841/how-do-i-concatenate-the-string-elements-of-my-array-into-a-single-string-in-c
char *concatenate(size_t size, char *array[size], const char *joint){
    size_t jlen, lens[size] ;
    size_t i, total_size = (size-1) * (jlen=strlen(joint)) + 1 ;
    char *result, *p ;
    // Get total_size
    for(i=0;i<size;++i){
        total_size += (lens[i]=strlen(array[i])) ;
    }//end for get total_size of concatenated string
    p = result = malloc(total_size) ;
    for(i=0;i<size;++i){
        memcpy(p, array[i], lens[i]) ;
        p += lens[i] ;
        if(i < size-1){
            memcpy(p, joint, jlen) ;
            p += jlen ;
        }//if not last string, add joint char
    }//end for concatenate strings into concatenate string
    *p = '\0' ;
    return result ;
}//end concatenate()



// Prints elements of cmdline struct in easier to read format
void printcmdline(cmdline cmd){
    printf("---Begin Command Line Struct---\n") ;
    printf("\tGlobal Variables\n");
    printf("followLinks: %d\n", cmd.followLinks) ;
    printf("depth: %d\n", cmd.depth) ;
    printf("maxdepth: %d\n", cmd.maxdepth) ;
    printf("newer: %ld\n", cmd.newer) ;
    printf("\tFilenames: %d\n", cmd.countFilenames) ;
    for(int i = 0 ; i < cmd.countFilenames ; i++){
        printf("filenames[%d]: %s\n", i, cmd.filenames[i]) ;
    }//end list filenames
    printf("\tTests and Args: %d\n", cmd.countExpressions) ;
    for(int i = 0 ; i < cmd.countExpressions ; i++){
        printf("%s\t%s\n", cmd.tests[i].name, cmd.tests[i].argument) ;
    }//end list expressions
    printf("\tActions and Args: %d\n", cmd.countActions);
    for(int i = 0 ; i < cmd.countActions ; i++){
        printf("%s\t%s\n", cmd.actions[i].action, cmd.tests[i].argument) ;
    }
}//end printcmdline



// Free cmdline struct (mem leak fix)
void freecmdline(cmdline cmd){
    for(int i = 0 ; i < cmd.countFilenames ; i++){
        free(cmd.filenames[i]) ;
    }//end for free filenames
    for(int i = 0 ; i < cmd.countExpressions ; i++){
        free(cmd.tests[i].name) ;
        free(cmd.tests[i].argument) ;
    }//end for free expressions
    for(int i = 0 ; i < cmd.countActions ; i++){
        free(cmd.actions[i].action) ;
        free(cmd.actions[i].argument) ;
    }
}// end freecmdline



// Check if string is a number, used to check -maxdepth arg
// Source: https://rosettacode.org/wiki/Determine_if_a_string_is_numeric#C
int isNumeric (const char * s){
    if(s == NULL || *s == '\0' || isspace(*s))
        return 0;
    char * p ;
    strtod (s, &p) ;
    return *p == '\0' ;
}//end isNumeric

/*
------------------ applyExpression() ---------------
*/

void applyExpression(cmdline cmd, char *path, int currentDepth, node node){
    DIR *dp = opendir(path) ;
    struct dirent *entry ;
    struct stat buf ;

    // check if directory
    if(!dp){
        return ;
    }//end if not directory

    // check maxdepth
    if(cmd.maxdepth > -1 && currentDepth > cmd.maxdepth){
        return ;
    }//end if over maxdepth

    // For every file in directory
    while((entry = readdir(dp))){
        // Construct new path to file for lstat and stat
        char *newpath = malloc(strlen(path) + strlen(entry->d_name) + 2) ;
        strcpy(newpath, path) ;
        strcat(newpath, "/") ;
        strcat(newpath, entry->d_name) ;
        // check "real" option
        if(cmd.followLinks){
            if(stat (newpath, &buf) < 0)
                DIE ("stat(%s) failed\n", newpath) ;
        }//end if stat()
        else{
            if(lstat (newpath, &buf) < 0)
                DIE ("lstat(%s) failed\n", newpath) ;
        }//end if lstat()
        node newnode = {
            .devid = buf.st_dev,
            .inodenum = buf.st_ino,
            .prev = node, 
        };
        // Run Tests
        bool passed = true ;
        if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) 
            passed = false ;
        if(cmd.newer)
            passed = (difftime(cmd.newer,buf.st_mtim.tv_sec) > 0) ;
        for(int i = 0 ; i < cmd.countExpressions ; i++){
            // -name
            if(strcmp("-name", cmd.tests[i].name) == 0){
                // debug("\ttesting -name: %s against %s", cmd.tests[i].argument, entry->d_name);
                if(fnmatch(cmd.tests[i].argument, entry->d_name, 0) != 0)
                    passed = false ;
            }//end if -name

            if (cmd.newer) {
                passed = difftime(buf.st_mtim.tv_sec, cmd.newer) > 0;
            }
        }//end testing
        //debug("\t%d for %s%s%s", passed, path, "/", entry->d_name);

        //Check if passed tests, therefore undergo actions
        if(passed){
            for(int i = 0 ; i < cmd.countActions ; i++){
                // -print
                if(strcmp(cmd.actions[i].action, "-print") == 0){
                    if(!cmd.depth)
                        printf("%s\n", newpath) ;
                    if(S_ISDIR(buf.st_mode) && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0){
                        applyExpression(cmd, newpath, currentDepth+1, newnode) ;
                    }//end if is subdir
                    else if(S_ISLNK(buf.st_mode)){
                        // check if in a loop here
                        // if not in loop

                    }//end if is symbolic link
                    if(cmd.depth)
                        printf("%s\n", newpath) ;
                }//end if -print
                // -exec
                if(strcmp(cmd.actions[i].action, "-exec")){
                    system(cmd.actions[i].argument) ;
                }//end if -exec
            }//end for every action
        }//end if passed, can therefore do actions
        free(newpath) ;
    }//end while reading every file in path
    closedir(dp) ;
}//end applyExpression


/*
----------------------- main() ---------------------
- Goes through argv[] to fill cmdline struct with
    elements as such. This struct will then be used
    with a recursive helper function similar to Dir.c
    with each filename passed into it. Helper function
    will then apply the expression.
----------------------- issues ---------------------
- Does not include -a, -o, and -print expression
*/
int main(int argc, char *argv[]){
    /*
    if (argc-1 == 0) {
        DIE("incorrect syntax\nsyntax: nfind [-P|-L]* [filename]* [-depth] [-maxdepth <number>] [-name <expression>] [-newer <file>] [-print] [-exec <cmd> \\;] [-o|-a]", NULL) ;
    }//end if missing all components
    */
    // Intialize cmdline stuct
    cmdline cmd = {
        .followLinks = false,
        .depth = false, 
        .maxdepth = -1, // -1 = unlimited, 0 = only current dir, 1 = 1 subdir deep, etc.
        .countFilenames = 0,
        .countExpressions = 0,
    };

    node node ={
        node.prev = NULL,
    };

    int i = 1 ;                     // iterator for the arguments

    // Get Options
    while(argv[i] && (strcmp(argv[i], "-L") == 0 || strcmp(argv[i], "-P") == 0)){
        cmd.followLinks = strcmp(argv[i], "-L") == 0 ;
        i++ ;
    }//end while get options

    // Get Filenames
    while(argv[i] && argv[i][0] != '-'){
        cmd.filenames[cmd.countFilenames] = strdup(argv[i]) ;   // freecmdline() deals with mem leaks here
        //strcpy(cmd.filenames[cmd.countFilenames], argv[i]) ;
        cmd.countFilenames++ ;
        i++ ;
    }//end while filenames

    // No filename listed, therefore "."
    if(cmd.countFilenames == 0){
        cmd.filenames[cmd.countFilenames] = strdup(".");
        cmd.countFilenames = 1 ;
    }//end if no filename listed, therefore use current root directory (".")

    // Get Expressions
    while(argv[i]){
        //debug("%3d: %s", i, argv[i]);

        // Check if valid expression
        if(strcmp(argv[i],"-maxdepth") == 0 || strcmp(argv[i],"-depth") == 0 || strcmp(argv[i], "-newer") == 0 || strcmp(argv[i], "-name") == 0 || strcmp(argv[i], "-print") == 0 || strcmp(argv[i], "-exec") == 0 || strcmp(argv[i], "-a") == 0 || strcmp(argv[i], "-o") == 0){
            char *name = strdup(argv[i]) ;
            char *value = "" ;

            // Arg needed after expression (not followed by another expression)
            if(strcmp(name, "-maxdepth") == 0 || strcmp(name, "-name") == 0 || strcmp(name, "-newer") == 0){
                if(argc >= i+1 && argv[i+1] && argv[i+1][0] != '-'){
                    value = strdup(argv[i+1]) ;
                    //debug("value: %s",value);
                    i++ ;
                }//end if has arg
                else{
                    DIE("missing argument after %s", name) ;
                }//end else missing arg
            }//end if arg needed for expression (-maxdepth, -name, -newer)

            // Set "global variables"
            if(strcmp(name, "-newer") == 0) {
                struct stat reference ;
                if(cmd.followLinks){
                    if(stat(value, &reference) < 0)
                        DIE("'%s': No such file or directory", value) ;
                }//end if stat()
                else{
                    if(lstat(value, &reference) < 0)
                        DIE("'%s': No such file or directory", value) ;
                }//end if lstat()

                //debug("checking newer for: %s", value);
                // store cmd.newer: if already exists get biggest value, otherwise set initial value
                if (cmd.newer) {
                    cmd.newer = difftime(cmd.newer,reference.st_mtim.tv_sec) > 0 ? cmd.newer : reference.st_mtim.tv_sec;
                    debug("updating newer: %ld", cmd.newer);
                } else {
                    cmd.newer = reference.st_mtim.tv_sec;
                    debug("initializing newer: %ld", cmd.newer);
                }
            }//end -newer case

            if(strcmp(name, "-depth") == 0){
                if(cmd.countExpressions > 0 || cmd.countActions)
                    WARN("option: %s listed after non-option expression component", name) ;
                cmd.depth = true ;
            }
            if(strcmp(name, "-maxdepth") == 0){
                if(cmd.countExpressions > 0 || cmd.countActions)
                    WARN("option: %s listed after non-option expression component", name) ;
                if(isNumeric(value)){
                    if(cmd.maxdepth > strtoul(value, NULL, 10))
                        cmd.maxdepth = strtoul(value, NULL, 10) ;
                }//end if correct arg for -maxdepth
                else{
                    DIE("invalid -maxdepth arg: %s\n", value) ;
                }//end if incorrect arg for -maxdepth
            }//end if -maxdepth

            // -exec case
            if(strcmp(name, "-exec") == 0){
                // Flag for making sure \; is found
                bool completeExec = false ;
                // Get cmd elements for system()
                char *execParts[argc] ;
                int execCount = 0 ;
                i++ ;
                while(argv[i] && !completeExec){
                    completeExec = (strcmp(argv[i], ";") == 0) ; // it's complete when we get to \; [in argv shown as regular ";" instead of "\;"" ]
                    execParts[execCount++] = strdup(argv[i]) ; // store that exec part
                    i++ ;
                }//end while get cmd elements
                // Check flag
                if(completeExec){
                    value = concatenate(execCount-1, execParts, " ") ;  // concatenate these parts (e.g. {"ls","-l","{}"} --> "ls -l {}")
                    // mem leak fix with strdup() for execParts[]
                    for(int j = 0 ; j < execCount ; j++){
                        free(execParts[j]) ;
                    }//end for free execParts
                }else{
                    value = "" ;
                    DIE("\\; not found after -exec", NULL) ;
                }//end if \; not found
            }//end -exec case

            // Add to expression array if needed
            if(strcmp(name,"-name") ==0){
                cmd.tests[cmd.countExpressions].name = name ;
                cmd.tests[cmd.countExpressions].argument = value ;
                cmd.countExpressions++ ;
            }//end add new expression stuct -name
            if(strcmp(name, "-print") == 0){
                cmd.actions[cmd.countActions].action = name ;
                cmd.countActions++ ;
            }//end if -print
            if(strcmp(name, "-exec") == 0){
                cmd.actions[cmd.countActions].action = name ;
                cmd.actions[cmd.countActions].action = value ;
                cmd.countActions++ ;
            }
        }//end if expression
        else{
            DIE("unexpected argument: %s", argv[i]) ;
        }//end if not an expression

        // todo: leads to segmentation fault if -exec is not terminated with \;
        i++ ; 
    }//end while get Expressions

    if(cmd.countActions == 0){
        cmd.actions[cmd.countActions].action = strdup("-print") ;
        cmd.countActions++ ;
    }//end base is print

    //if (DEBUG) printcmdline(cmd) ;

    // Apply tests to filenames listed first, if passed then use helper function to recursively test
    for(i = 0 ; i < cmd.countFilenames ; i++){
        //debug("--- for: %s", cmd.filenames[i]);
        // Apply tests and action to filename, then pass recursively into helper if directory
        struct stat filename ;
        if(cmd.followLinks){
            if(stat (cmd.filenames[i], &filename) < 0)
                DIE ("stat(%s) failed\n", cmd.filenames[i]) ;
        }//end if stat()
        else{
            if(lstat (cmd.filenames[i], &filename) < 0)
                DIE ("lstat(%s) failed\n", cmd.filenames[i]) ;
        }//end if lstat()
        // Run Tests
        bool passed = true ;
        for(int i = 0 ; i < cmd.countExpressions ; i++){
            // -name
            if(strcmp("-name", cmd.tests[i].name) == 0){
                // debug("\ttesting -name: %s against %s", cmd.tests[i].argument, entry->d_name);
                if(fnmatch(cmd.tests[i].argument, cmd.filenames[i], 0) != 0)
                    passed = false ;
            }//end if -name

            if (cmd.newer) {
                passed = difftime(filename.st_mtim.tv_sec, cmd.newer) > 0;
            }
        }//end testing
        //debug("\t%d for %s%s%s", passed, path, "/", entry->d_name);

        //Check if passed tests, therefore undergo actions
        if(passed){
            for(int i = 0 ; i < cmd.countActions ; i++){
                // -print
                if(strcmp(cmd.actions[i].action, "-print") == 0){
                    if(!cmd.depth)
                        printf("%s\n", cmd.filenames[i]) ;
                    if(S_ISDIR(filename.st_mode) != 0){
                        applyExpression(cmd, cmd.filenames[i], 0, node) ;
                    }//end if is subdir
                    else if(S_ISLNK(filename.st_mode)){
                        printf("%s@\n", cmd.filenames[i]) ;
                    }//end if is symbolic link
                    if(cmd.depth)
                        printf("%s\n", cmd.filenames[i]) ;
                }//end if -print
                // -exec
                if(strcmp(cmd.actions[i].action, "-exec")){
                    system(cmd.actions[i].argument) ;
                }//end if -exec
            }//end for every action
        }//end if passed, can therefore do actions
    }//end for each path/filename
    // Free memory leaks
    freecmdline(cmd) ;
}//end main
