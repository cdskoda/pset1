#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(void) {
	// touch fileOld.txt; sleep 3; touch fileNew.txt

    struct stat sf1, sf2 ;

	stat("fileOld.txt", &sf1);
	stat("fileNew.txt", &sf2);


    printf("newer sf1: %lld\n", (long long) sf1.st_mtimespec.tv_sec) ;
    printf("newer sf2: %lld\n", (long long) sf2.st_mtimespec.tv_sec) ;

	printf("diff: %f\n", difftime(sf1.st_mtimespec.tv_sec, sf2.st_mtimespec.tv_sec));

}
