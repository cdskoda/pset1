#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <fnmatch.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>
#include <errno.h>

// if make; then ./parseArguments; fi

#define DEBUG 1

#define ANSI_COLOR_YELLOW    "\x1b[33m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define debug(fmt, ...) do { if (DEBUG) fprintf(stderr, fmt "\n", __VA_ARGS__); } while (0)
#define WARN(format,...) fprintf (stderr, ANSI_COLOR_YELLOW "warning: " ANSI_COLOR_RESET format "\n", __VA_ARGS__)
#define DIE(format,...) fprintf (stderr, ANSI_COLOR_YELLOW "error: " ANSI_COLOR_RESET format "\n", __VA_ARGS__), exit (EXIT_FAILURE)

// fixed array sizes (ideally: dynamically sized)
#define ARRAYSIZE 100 

// ---------------------------------------------------------------------------

// https://stackoverflow.com/questions/25767841/how-do-i-concatenate-the-string-elements-of-my-array-into-a-single-string-in-c

// concatenates an array of strings into single string
char *concatenate(size_t size, char *array[size], const char *joint){
    size_t jlen, lens[size];
    size_t i, total_size = (size-1) * (jlen=strlen(joint)) + 1;
    char *result, *p;

    for(i=0;i<size;++i){
        total_size += (lens[i]=strlen(array[i]));
    }
    p = result = malloc(total_size);
    for(i=0;i<size;++i){
        memcpy(p, array[i], lens[i]);
        p += lens[i];
        if(i<size-1){
            memcpy(p, joint, jlen);
            p += jlen;
        }
    }
    *p = '\0';
    return result;
}

// ********************************************************************************************
// general remark: check and fix all memory related actions - not at all sure if done correctly
// strdup vs strcopy: https://stackoverflow.com/a/14021507/678951
// ********************************************************************************************


// ---------------------------------------------------------------------------

// store all cmd line elements in a struct
typedef struct cmdline {
	bool followLinks; // for a file: use lstat (-P, followLinks == false) or stat (-L, followLinks == true)
	bool depth; // depth = true: first recursively through folders, then the files; depth = false: first the files, then recursive folders
	int maxdepth; // how many directories deep? 0 = current dir only, 1 = one subdirectory deep
	char *filenames[ARRAYSIZE]; // array of filenames (root directories to search)
	struct {
		char *name;
		char *argument;
	} expressions[ARRAYSIZE]; // e.g. { .name = -newer, .argument = file1 }
	int countFilenames;
	int countExpressions;	
} cmdline;

// =============================================================================

int main(int argc, char *argv[]){

	// ---------------------------------------------------------------------------
	// part 1: process the arguments --> results in cmd structure
	// ---------------------------------------------------------------------------

	// simulate argc and argv (argcx and argvx)
	const char * argvx[] = {
		// for testing: enable/disable certain rows
		"-P","-L","-P","-L",
		"filename1","filename2",
		"-depth", "-name","*a", "-o", "-newer","file1", "-o", "-newer", /* "file2", */ // omit -newer parameter
		"-exec", "ls","-l","{}","\\;",
		"-print", "this_should_give_syntax_error_or_be_ignored", "-maxdepth","1",
		NULL
	};
	const int argcx = sizeof(argvx) / sizeof(argvx[0]);
	debug("# of arguments: %d", argcx-1);

	if (argcx-1 == 0) {
		DIE("incorrect syntax\nsyntax: nfind [-P|-L]* [filename]* [-depth] [-maxdepth <number>] [-name <expression>] [-newer <file>] [-print] [-exec <cmd> \\;] [-o|-a]",NULL);
	}

	// initialize the cmd line struct, with default values
	cmdline cmd = {
		.followLinks = false, // if symlink: get attribute of symlink, not of linked file
		.depth = false, // files first
		.maxdepth = -1, // no limit
		.countFilenames = 0,
		.countExpressions = 0,
	};

	int i=0; // iterator for the arguments

	debug("----- listing the arguments -----", NULL);

	// print the arguments
	for(int i = 0; i < argcx; i++) {
		debug("%3d: %s", i, argvx[i]);
	}

	debug("----- processing the arguments -----", NULL);

	// options
	while( strcmp(argvx[i], "-L") == 0 || strcmp(argvx[i], "-P") == 0 ) {
		cmd.followLinks = strcmp(argvx[i], "-L") == 0;
		i++;
	}

	debug(">>> followLinks: %d", cmd.followLinks);

	// filenames
	while( argvx[i][0] != '-' ) {
		debug("%3d: filename: %s", i, argvx[i]);
		cmd.filenames[cmd.countFilenames] = strdup(argvx[i]);
		cmd.countFilenames++;
		i++;
	}

	// use default filename if no filename was provided
	if (cmd.countFilenames == 0) {
		cmd.filenames[cmd.countFilenames] = strdup(".");
        cmd.countFilenames = 1 ;
	}

	debug(">>> filenames: %d", cmd.countFilenames);

	// expressions
	while( argvx[i] ) {
		// expressions start with -
		if (argvx[i][0] == '-') {
			debug("%3d: expression %d: %s", i, cmd.countExpressions, argvx[i]);

			char *name = strdup(argvx[i]);
			char *value = "";

			// the expressions that expect an argument value
			if (strcmp(name, "-maxdepth") == 0 || strcmp(name, "-name") == 0 || strcmp(name, "-newer") == 0) {
				// make sure there is a next argument, and that it is not an expression (=starts with -)
				if (argcx >= i+1 && argvx[i+1] && argvx[i+1][0] != '-') {
					value = strdup(argvx[i+1]);
					debug("\tvalue: %s", value);
					i++; // we have an argument, so we can move to the next one
				} else {
					WARN("missing argument after %s", name);
					// don't add this expression and go to the next argument (alternative: add the expression and let part 2 deal with it)
					i++;
					continue;
				}
			}

			// store depth/maxdepth as "global" variable
			if (strcmp(name, "-maxdepth") == 0) cmd.maxdepth = strtoul(value, NULL, 10);
			if (strcmp(name, "-depth") == 0) cmd.depth = true;

			// special case: -exec cmd can include -, e.g. "-exec ls -l {} \;"
			if (strcmp(name, "-exec") == 0) {
				debug("\treading ahead until end of -exec", NULL);

				// keep track of having a complete exec command (=terminated by \;)
				bool completeExec = false;

				// store the exec cmd elements
				char *execParts[argcx];
				int execCount = 0;

				// keep reading arguments until we have a complete exec (or end of arguments)
				while( argvx[i] && !completeExec ) {
					i++;
					completeExec = strcmp(argvx[i], "\\;") == 0; // it's complete when we get to \;
					execParts[execCount++] = strdup(argvx[i]); // store that exec part
					debug("\t%d - parts: %s", execCount, argvx[i]); 
				}

				if (completeExec) {
					value = concatenate(execCount-1, execParts, " "); // concatenate these parts (e.g. {"ls","-l","{}"} --> "ls -l {}")
				} else {
					value = "";
					DIE("\\; not found after -exec",NULL);
				}
			}

			// add a new expression struct with name+value (unless it is maxdepth or depth)
			if (strcmp(name,"-maxdepth") !=0 && strcmp(name,"-depth") != 0) {
				cmd.expressions[cmd.countExpressions].name = name;
				cmd.expressions[cmd.countExpressions].argument = value;
				cmd.countExpressions++;
			}

		} else {
			WARN("unexpected argument: %s", argvx[i]);
		}

		// todo: leads to segmentation fault if -exec is not terminated with \;

		i++; 
	}

	// todo: check what can / needs to be freed

	debug(">>> expressions: %d", cmd.countExpressions);

	// ---------------------------------------------------------------------------
	// part 2: loop through the filenames
	// ---------------------------------------------------------------------------

	debug("----- ready to loop through the files -----", NULL);

	debug("followLinks: %s", cmd.followLinks ? "true" : "false");
	debug("depth: %s", cmd.depth ? "true" : "false");
	debug("maxdepth: %d", cmd.maxdepth);

	// loop through each of the filenames
	for(int i = 0; i < cmd.countFilenames; i++) {
		debug("filename %d: '%s'", i, cmd.filenames[i]);

		// for each filename: loop through the expressions
		for(int j = 0; j < cmd.countExpressions; j++) {
			debug("\t%d: %s %s", j, cmd.expressions[j].name, cmd.expressions[j].argument);
		}
	};

}

